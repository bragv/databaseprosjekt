# parkett
rowLength = [18, 16, 17, 18, 18, 17, 18, 17, 17, 14]
for row in range(10):
    for seat in range(rowLength[row]):
        print(f"({seat+1}, {row+1}, 'parkett', 1),")

# balkong
rowLength = [28, 27, 22, 17]
for row in range(4):
    for seat in range(rowLength[row]):
        print(f"({seat+1}, {row+1}, 'balkong', 1),")

# galleri
rowLength = [33, 18, 17]
for row in range(3):
    for seat in range(rowLength[row]):
        print(f"({seat+1}, {row+1}, 'galleri', 1),")

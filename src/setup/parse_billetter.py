import sqlite3

def parse_billeter(connection, file, salId, uniqueSeatNum, omraaderWithoutRowNums = []):
    file = open(file, "r")
    lineCapture = file.readlines()
    file.close()

    dato = lineCapture[0].split(" ")[1].strip()

    omraader = []
    for line in lineCapture[1:]:
        if line[0] not in ["0", "1", "x"]:
            omraader.append((line.lower().strip(), []))
        else:
            omraader[-1][1].append(line)

    cursor = connection.cursor()
    cursor.execute("""
        SELECT forestillingsId, stykkeId
        FROM forestilling
        JOIN stykke USING (stykkeId)
        JOIN sal USING (salId)
        WHERE dato = ? AND salId = ?
    """, (dato, salId))
    forestillingsId, stykkeId = cursor.fetchone()

    seatNum = 1
    rowNum = 1
    for omraade in reversed(omraader):
        if omraade[0] in omraaderWithoutRowNums:
            rowNum = None
        else:
            rowNum = 1

        for rad in reversed(omraade[1]):
            if not uniqueSeatNum and rowNum != None:
                seatNum = 1

            for seat in rad:
                if seat == "1":
                    cursor.execute("""
                        INSERT INTO billett (seteNr, radNr, omraade, salId,
                            forestillingsId, type, stykkeId, kjopId)
                        VALUES (?, ?, ?, ?, ?, null, ?, null);
                        """, (seatNum, rowNum, omraade[0], salId,
                            forestillingsId, stykkeId ))
                seatNum += 1

            if rowNum != None:
                rowNum += 1

    connection.commit()

if __name__ == "__main__":
    connection = sqlite3.connect("teater.db")
    parse_billeter(connection, "textinput/gamle-scene.txt", 1, False)
    parse_billeter(connection, "textinput/hovedscenen.txt", 2, True, ["galleri"])
    connection.close()

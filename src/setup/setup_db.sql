-- DROP TABLE kundeprofil;
-- DROP TABLE billettkjop;
-- DROP TABLE billett;
-- DROP TABLE billettype;
-- DROP TABLE stykke;
-- DROP TABLE sal;
-- DROP TABLE akt;
-- DROP TABLE rolle;
-- DROP TABLE opptreri;
-- DROP TABLE ansatt;
-- DROP TABLE spiller;
-- DROP TABLE oppgave;
-- DROP TABLE jobbermed;
-- DROP TABLE sete;
-- DROP TABLE forestilling;
-- DROP TABLE sesong;

CREATE TABLE
	kundeProfil (
		tlfNr CHAR(8) not null, -- kun norske tlfnr?
		navn VARCHAR(64) not null,
		adresse VARCHAR(64) not null,
		CONSTRAINT kunde_pk PRIMARY key (tlfNr)
	);

CREATE TABLE
	billettKjop (
		kjopId INTEGER not null,
		dato DATE not null,
		tid TIME not null,
		tlfNr CHAR(8),
		CONSTRAINT kjop_pk PRIMARY key (kjopId),
		CONSTRAINT kjop_fk1 FOREIGN key (tlfNr) REFERENCES KundeProfil (tlfNr)
			ON UPDATE CASCADE
			ON DELETE SET null
	);

CREATE TABLE
	billett (
		seteNr INTEGER,
		radNr INTEGER,
		omraade VARCHAR(64),
		forestillingsId INTEGER not null,
		salId INTEGER not null,
		type VARCHAR(16),
		stykkeId INTEGER,
		kjopId INTEGER,
		CONSTRAINT billett_pk PRIMARY key (seteNr, radNr, omraade, salId, forestillingsId),
		CONSTRAINT billett_fk1 FOREIGN key (seteNr, radNr, omraade, salId) REFERENCES Sete (seteNr, radNr, omraade, salId)
			ON UPDATE CASCADE
			ON DELETE CASCADE,
		CONSTRAINT billett_fk2 FOREIGN key (type, stykkeId) REFERENCES Bilettype (type, stykkeId)
			ON UPDATE CASCADE
			ON DELETE RESTRICT,
		CONSTRAINT billett_fk3 FOREIGN key (kjopId) REFERENCES BilettKjop (kjopId)
			ON UPDATE CASCADE
			ON DELETE CASCADE,
		CONSTRAINT billett_fk4 FOREIGN key (forestillingsId) REFERENCES Forestilling (forestillingsId)
			ON UPDATE CASCADE
			ON DELETE CASCADE
	);

CREATE TABLE
	billettype (
		type VARCHAR(16) not null,
		stykkeId INTEGER not null,
		pris INTEGER not null,
		CONSTRAINT billettype_pk PRIMARY key (type, stykkeId),
		CONSTRAINT billettype_fk1 FOREIGN key (stykkeId) REFERENCES Stykke (stykkeId)
			ON UPDATE CASCADE
			ON DELETE CASCADE
	);

CREATE TABLE
	stykke (
		stykkeId INTEGER not null,
		tittel VARCHAR(256) not null,
		sesongId INTEGER not null,
		salId INTEGER not null,
        forfatter VARCHAR(64),
		CONSTRAINT stykke_pk PRIMARY key (stykkeId),
		CONSTRAINT stykke_fk1 FOREIGN key (sesongId) REFERENCES Sesong (sesongId)
			ON UPDATE CASCADE
			ON DELETE CASCADE,
		CONSTRAINT stykke_fk2 FOREIGN key (salId) REFERENCES Sal (salId)
			ON UPDATE CASCADE
			ON DELETE CASCADE
	);

CREATE TABLE
	sal (
		salId INTEGER not null,
		navn VARCHAR(256),
		antallPlasser INTEGER,
		CONSTRAINT sal_pk PRIMARY key (salId)
	);

CREATE TABLE
	akt (
		nr INTEGER not null,
		stykkeId INTEGER not null,
		tittel VARCHAR(256),
		CONSTRAINT akt_pk PRIMARY KEY (nr, stykkeId),
		CONSTRAINT akt_fk1 FOREIGN KEY (stykkeId) REFERENCES Stykke (stykkeId)
			ON UPDATE CASCADE
			ON DELETE CASCADE
	);

CREATE TABLE
	rolle (
		rolleId INTEGER not null,
		navn VARCHAR(64),
		CONSTRAINT rolle_pk PRIMARY KEY (rolleId)
	);

CREATE TABLE
	opptrerI (
		nr INTEGER not null,
		stykkeId INTEGER not null,
		rolleId INTEGER not null,
		CONSTRAINT opptrerI_pk PRIMARY KEY (nr, rolleId, stykkeId),
		CONSTRAINT opprerI_fk1 FOREIGN KEY (nr, stykkeId) REFERENCES Akt (nr, stykkeId)
			ON UPDATE CASCADE
			ON DELETE CASCADE,
		CONSTRAINT opprerI_fk2 FOREIGN KEY (rolleId) REFERENCES Rolle (rolleId)
			ON UPDATE CASCADE
			ON DELETE CASCADE
	);

CREATE TABLE
	ansatt (
		ansattId INTEGER not null,
		navn VARCHAR(64) not null,
		epost VARCHAR(64),
		ansattStatus VARCHAR(20) not null,
		erDirektør BOOLEAN not null,
		CONSTRAINT ansatt_pk PRIMARY KEY (ansattId)
	);

CREATE TABLE
	spiller (
		ansattId INTEGER not null,
		rolleId INTEGER not null,
		CONSTRAINT spiller_pk PRIMARY KEY (ansattId, rolleId),
		CONSTRAINT spiller_fk1 FOREIGN KEY (ansattId) REFERENCES Ansatt (ansattId)
			ON UPDATE CASCADE
			ON DELETE CASCADE,
		CONSTRAINT spiller_fk2 FOREIGN KEY (rolleId) REFERENCES Rolle (rolleId)
			ON UPDATE CASCADE
			ON DELETE CASCADE
	);

CREATE TABLE
	oppgave (
		tittel VARCHAR(64) not null,
		stykkeId INTEGER not null,
		CONSTRAINT oppgave_pk PRIMARY KEY (tittel, stykkeId),
		CONSTRAINT oppgave_fk1 FOREIGN KEY (stykkeId) REFERENCES Stykke (stykkeId)
			ON UPDATE CASCADE
			ON DELETE CASCADE
	);

CREATE TABLE
	jobberMed (
		ansattId INTEGER not null,
		tittel VARCHAR(64) not null,
		stykkeId INTEGER not null,
		CONSTRAINT jobberMed_pk PRIMARY KEY (ansattId, tittel, stykkeId),
		CONSTRAINT jobberMed_fk1 FOREIGN KEY (ansattId) REFERENCES Ansatt (ansattId)
			ON UPDATE CASCADE
			ON DELETE CASCADE,
		CONSTRAINT jobberMed_fk2 FOREIGN KEY (tittel, stykkeId) REFERENCES Oppgave (tittel, stykkeId)
			ON UPDATE CASCADE
			ON DELETE CASCADE
	);

CREATE TABLE
	sete (
		seteNr INTEGER,
		radNr INTEGER,
		omraade VARCHAR(64),
		salId INTEGER not null,
		CONSTRAINT sete_pk PRIMARY KEY (seteNr, radNr, omraade, salId)
	);

CREATE TABLE
	forestilling (
		forestillingsId INTEGER not null,
		dato DATE not null,
		tidspunkt TIME not null,
		stykkeId INTEGER not null,
		CONSTRAINT forestilling_pk PRIMARY KEY (forestillingsId)
        CONSTRAINT forestilling_fk FOREIGN KEY (stykkeId) REFERENCES stykke (stykkeId)
            ON UPDATE CASCADE
            ON DELETE CASCADE
	);

CREATE TABLE
	sesong (
		sesongId INTEGER not null,
		navn VARCHAR(64) not null,
		aar INTEGER not null,
		CONSTRAINT sesong_pk PRIMARY KEY (SesongId)
	);

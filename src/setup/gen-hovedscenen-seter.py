# parkett
seatCount = 0
for row in range(1, 18+1):
    for seatIndex in range(1, 28+1):
        seatCount += 1
        if row in [17, 18] and seatIndex in [19, 20, 21, 22]:
            continue
        print(f"({seatCount}, {row}, 'parkett', 2),")

# galleri
for _ in range(20):
    seatCount += 1
    print(f"({seatCount}, NULL, 'galleri', 2),")

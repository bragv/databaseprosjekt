import random
from queries import *

def genereateUniqueKjopId(connection):
    cursor = connection.cursor()
    for _ in range(10000000):
        kjopId = random.randint(0,10000000)
        cursor.execute("""
            SELECT *
            FROM billettKjop
            WHERE kjopId = ?;
            """, (kjopId, ))
        result = cursor.fetchone()
        if result == None:
            return kjopId
    return None

def make_purchase(connection, seter, billettype, forestillingsId, tlfNr, dato, tid):
    cursor = connection.cursor()
    stykkeId = getStykke(connection, forestillingsId)
    kjopId = genereateUniqueKjopId(connection)

    # set inn kjop
    cursor.execute("""
        INSERT INTO billettKjop (kjopId, dato, tid, tlfNr)
        VALUES (?, ?, ?, ?);
        """, (kjopId, dato, tid, tlfNr))

    # sett inn billetter
    for sete in seter:
        cursor.execute("""
            INSERT INTO billett (seteNr, radNr, omraade, salId,
            forestillingsId, type, stykkeId, kjopId)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?);
            """, (sete[0],sete[1],sete[2],sete[3], forestillingsId, billettype, stykkeId, kjopId ))

    connection.commit()
    return kjopId

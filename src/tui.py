import re
from queries import *
from insertions import *

def user_query(alternatives, keys = None):
    while True:
        for index, alternative in enumerate(alternatives):
            print(f"[{index+1}]: {alternative}")
        ans = int_input()-1

        if not ans in range(len(alternatives)):
            print(f"venligst oppgi et tall mellom 1 og {len(alternatives)}")
            continue

        if keys == None:
            return alternatives[ans]
        return keys[ans]

def int_input():
    while True:
        ans = input("> ")
        if not ans.isdigit():
            print("vennligst oppgi et tall")
            continue
        return int(ans)

def enter_date():
    dateFromat = re.compile("\d{4}-\d{2}-\d{2}")
    while True:
        y = input("År  (yyyy): ")
        m = input("Måned (mm): ")
        d = input("Dag   (dd): ")
        result = f"{y}-{m}-{d}"
        if dateFromat.match(result):
            return result
        print("Vennligs oppgi dato på riktig format")


def print_seats(seats):
    print("seteNr   rad      Område")
    for seat in seats:
        seat = list(seat)
        if seat[0] == None: seat[0] = "-"
        if seat[1] == None: seat[1] = "-"
        if seat[2] == None: seat[2] = "-"
        print(f"{seat[0]:<9}{seat[1]:<9}{seat[2]:<9}")

def print_skuespillere(skuespillere):
    print("Skuespiller                   Stykke                             Rolle")
    for skuespiller in skuespillere:
        print(f"{skuespiller[0]:<30}{skuespiller[1]:<35}{skuespiller[2]}")

def print_solgte_forestillinger(forestillinger):
    print("Forestilling                   Dato                   Antall biletter solgt")
    for forestilling in forestillinger:
        print(f"{forestilling[0]:<30}{forestilling[1]:<20}{forestilling[2]}")

def order_tickets(connection, sesongId):
    tlfNr = "99999999" # Hardkodet eksempelbruker
    print(f"logget in med {tlfNr}")

    forestillinger = getForestillingerISesong(connection, sesongId)
    if not forestillinger:
        print("fant ingen forestillinger, kjøp avbrutt")
        return
    print("Velg forestilling")
    forestillingsId = user_query(
        list(map(lambda tup: f"{tup[1]}, {tup[2]} kl. {tup[3]}", forestillinger)),
        list(map(lambda tup: tup[0], forestillinger))
    )

    types = getTypes(connection, forestillingsId)
    if types == None:
        print("Fant ingen billettyper, kjøp avbrutt")
        return
    print("Velg bilettype")
    billettype = user_query(
        list(map(lambda tup: f"{tup[0]}, {tup[1]} kr", types)),
        list(map(lambda tup: tup[0], types))
    )

    print("Oppgi antall billetter")
    amount = int_input()
    if "gruppe" in billettype.lower() and amount < 10: # det antas at alle gruppebilletter grupper på minst 10, 
        print("Ugyldig antall biletter, kjøp avbrutt")
        return

    seats = getAvalibleSeats(connection, forestillingsId, amount)
    if seats == None:
        print("fant ingen seter, kjøp avbrutt")
        return
    print("Fant seter:")
    print_seats(seats)

    print("bekreft kjøp?")
    ans = user_query(["yes", "no"])
    if ans == "yes":
        dato = "2024-01-01" # hardkodet eksempel
        tid = "00:00" # hardkodet eksempel
        kjopId = make_purchase(connection, seats, billettype, forestillingsId, tlfNr, dato, tid)
        print(f"kjøp gjennomført, totalpris: {getPrice(connection, kjopId)} kr")
    return

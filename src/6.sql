SELECT
  stykke.tittel AS stykke,
  forestilling.dato,
  COUNT(billett.forestillingsId) AS "antall billetter"
FROM
  forestilling
  JOIN stykke USING (stykkeId)
  JOIN billett USING (forestillingsId)
GROUP BY
  forestillingsId
ORDER BY
  "antall billetter" DESC;

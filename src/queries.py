def getForestillingPåDato(connection, date):
    cursor = connection.cursor()
    cursor.execute("""
        SELECT forestillingsId, stykke.tittel, forestilling.tidspunkt, COUNT(billett.forestillingsId)
        FROM stykke
        JOIN forestilling USING (stykkeId)
        LEFT OUTER JOIN billett USING (forestillingsId)
        WHERE forestilling.dato = ?
        GROUP BY forestilling.forestillingsId;
        """, (date,))

    return cursor.fetchall()

def getForestillingerISesong(connection, sesongId):
    cursor = connection.cursor()
    cursor.execute("""
        SELECT forestillingsId, stykke.tittel, dato, tidspunkt
        FROM forestilling
        JOIN stykke USING (stykkeId)
        WHERE sesongId = ?
        ORDER BY dato, tidspunkt
        """, (sesongId, ))

    return cursor.fetchall()

def getSal(connection, forestillingsID):
    cursor = connection.cursor()
    cursor.execute("""
        SELECT salID
        FROM forestilling
        JOIN stykke USING (stykkeID)
        JOIN sal USING (salID)
        WHERE forestillingsID = ?
        """, (forestillingsID,))
    return cursor.fetchone()[0]

def getStykke(connection, forestillingsID):
    cursor = connection.cursor()
    cursor.execute("""
        SELECT stykkeId
        FROM forestilling
        JOIN stykke USING (stykkeID)
        WHERE forestillingsID = ?
        """, (forestillingsID,))
    return cursor.fetchone()[0]

def getVacantSeatsOnRow(connection, radNr, salId, omraade, forestillingsId):
    cursor = connection.cursor()
    cursor.execute("""
        SELECT seteNr, radNr, omraade, salId
        FROM sete
        WHERE radNr = ? AND salId = ? AND omraade = ? AND
        NOT EXISTS (
            SELECT *
            FROM billett
            WHERE billett.seteNr = sete.seteNr AND billett.radNr = ? 
            AND billett.omraade = ? AND forestillingsId = ?
        )
        ORDER BY seteNr;
        """, (radNr, salId, omraade, radNr, omraade, forestillingsId))
    return cursor.fetchall()

def getAvalibleSeats(connection, forestillingsId, amount):
    salId = getSal(connection, forestillingsId);

    # prefer seats on same rad and omraade
    for rad, omraade in getRows(connection, salId):
        seter = getVacantSeatsOnRow(connection, rad, salId, omraade, forestillingsId)
        if len(seter) >= amount:
            return seter[:amount]


    # find avalible seats even if not on same rad and omraade
    cursor = connection.cursor()
    cursor.execute("""
        SELECT seteNr, radNr, omraade, salId
        FROM sete
        WHERE salId = ?
        AND (seteNr, radNr, omraade) NOT IN (
            SELECT seteNr, radNr, omraade
            FROM billett JOIN forestilling USING (forestillingsId)
            WHERE forestillingsId = ?
        )
        ORDER BY radNr, omraade, seteNr;
        """, (salId, forestillingsId))
    seter = cursor.fetchmany(amount)
    if len(seter) == amount:
        return seter

    # fewer than amount seter available
    return None

def getRows(connection, salId):
    cursor = connection.cursor()
    cursor.execute("""
        SELECT DISTINCT radNr, omraade 
        FROM sete
        WHERE salId = ?;
        """, (salId,))
    return cursor.fetchall()

def getPrice(connection, kjopId):
    cursor = connection.cursor()
    cursor.execute("""
        SELECT SUM(billettype.pris)
        FROM billett
        JOIN billettype USING (type, stykkeId)
        WHERE billett.kjopId = ?;
        """, (kjopId,))
    return cursor.fetchone()[0]

def getTypes(connection, forestillingsId):
    cursor = connection.cursor()
    cursor.execute("""
        SELECT type, pris
        FROM billettype
        JOIN stykke USING (stykkeId)
        JOIN forestilling USING (stykkeId)
        WHERE forestillingsId = ?;
        """, (forestillingsId,))
    return cursor.fetchall()

def getActorsWhoPlayedInSameAct(connection, actor):
    cursor = connection.cursor()

    cursor.execute("""
        SELECT DISTINCT a1.navn, stykke.tittel
        FROM ansatt a1
        JOIN spiller s1 USING (ansattId)
        JOIN rolle r1 USING (rolleId)
        JOIN OpptrerI o1 USING (rolleId)
        JOIN akt USING (nr, stykkeId)
        JOIN OpptrerI o2
            ON (akt.nr, akt.stykkeId) = (o2.nr, o2.stykkeID)
        JOIN rolle r2
            ON o2.rolleId = r2.rolleId
        JOIN spiller s2
            ON r2.rolleId = s2.rolleId
        JOIN ansatt a2
            ON s2.ansattId = a2.ansattId
        JOIN stykke USING (stykkeId)
        WHERE a2.navn = ?
        AND a1.ansattID != a2.ansattID
        ORDER BY stykke.tittel, a1.navn;
        """, (actor, ))

    return cursor.fetchall()

SELECT DISTINCT
  ansatt.navn AS skuespiller,
  stykke.tittel AS stykke,
  rolle.navn AS rolle
FROM
  ansatt
  JOIN spiller USING (ansattID)
  JOIN rolle USING (rolleId)
  JOIN opptrerI USING (rolleId)
  JOIN akt USING (nr, stykkeId)
  JOIN stykke USING (stykkeId)

#!/bin/sh
cd setup/
echo "===============setup_db.sql======================"
sqlite3 teater.db < setup_db.sql
echo "===============insert_values.sql================="
sqlite3 teater.db < insert_values.sql
echo "===============parse-billetter.py================"
python3 parse_billetter.py
echo "===============Brukerhistorier==================="
mv teater.db ../
cd ../
echo "Brukerhistorie 5: Skuespillere i teateret"
sqlite3 --table teater.db < 5.sql
echo "Brukerhistorie 6: Mest solgte forestillinger"
sqlite3 --table teater.db < 6.sql
echo "===============staring app======================="
python3 app.py teater.db

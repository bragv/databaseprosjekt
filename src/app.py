import sys
import sqlite3
from queries import *
from tui import *

def run(database):
    connection = sqlite3.connect(database)
    sesongId = 1 # hardkodet eksempel

    alternativ = [
        "bestill billett",
        "se forestillinger",
        "se medskuespillere",
        "avslutt",
    ]
    while True:
        print("\n=============\n")
        handling = user_query(alternativ)

        if handling == "bestill billett":
            order_tickets(connection, sesongId)

        elif handling == "se forestillinger":
            date = enter_date()
            forestillinger = getForestillingPåDato(connection, date)
            if forestillinger != None:
                for f in forestillinger:
                    print(f"{f[1]}, {f[2]}: {f[3]} billetter kjøpt.")
            else:
                print("Fant ingen forestillinger på denne datoen")

        elif handling == "se medskuespillere":
            print("skriv navn på skuespiller")
            navn = input("> ")
            skuespillere = getActorsWhoPlayedInSameAct(connection, navn)
            if skuespillere != None:
                print(f"Skuespillere som har spilt i samme akt som {navn}:")
                for s in skuespillere:
                    print(f"{s[0]} i {s[1]}.")
            else:
                print(f"Fant ingen skuespillere med navn {navn}")

        elif handling == "avslutt":
            break

    connection.close()

if __name__ == "__main__":
    run(sys.argv[1])
